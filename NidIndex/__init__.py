from aqt import mw
from anki import hooks
from anki.template import TemplateRenderContext


def add_text_to_index_field(field_text: str,
                            field_name: str,
                            filter_name: str,
                            context: TemplateRenderContext) -> str:
    if not filter_name == "Index":
        if field_text is None:
            return ""
        return field_text
    else:
        note = context.note()
        for (name, value) in note.items():
            if name is not None and value is not None:
                if name == "CID_Index" and value != "":
                    try:
                        cid = str(value).lstrip()[4:]
                        index_card = mw.col.get_card(int(cid))
                        html_str = ""
                        if index_card.question() is not None:
                            html_str += f""" 
                            <div style='text-align: center;'>
                                {index_card.question()}
                            </div>
                            """
                        else:
                            html_str += f""" 
                            <div style='text-align: center;'>
                                No question in Index card
                            </div>
                            """
                        if index_card.answer() is not None:
                            html_str += f"""
                            <div style='text-align: center;'>
                                {index_card.answer()}
                            </div>
                            """
                        else:
                            html_str += f""" 
                            <div style='text-align: center;'>
                                No answer in Index card
                            </div>
                            """
                        html_str += f"""
                        <br>
                        CID to edit: cidd{cid}
                        """
                        return html_str
                    except Exception as e:
                        return "Error in finding CID. Check the CID value. Error: " + str(e)


hooks.field_filter.append(add_text_to_index_field)
